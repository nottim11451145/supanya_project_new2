﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    public static AudioClip jumpSound, deadSound;
    static AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        jumpSound = Resources.Load<AudioClip>("Jump");
        deadSound = Resources.Load<AudioClip>("Dead");

        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "Jump": audioSource.PlayOneShot(jumpSound);
                break;

            case "Dead":
                audioSource.PlayOneShot(deadSound);
                break;

        }
    }
}
